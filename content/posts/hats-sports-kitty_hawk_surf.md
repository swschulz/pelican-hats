Title: Kitty Hawk Surf Company and Hurley
Date: 2014-06-23 09:07
Category: Hats
Tags: Sports, Surfing
Slug: kitty-hawk-surf-company
Author: Grumpy

We spent our annual family vacation on North Carolina's Outer Banks again this year.  And while surfing is not huge there, it seems to have been the theme for my hat purchases this year.

First up is a beat up looking snapback from the [Kitty Hawk Surf Company](http://www.khsurf.com).  Super comfortable.

![Kitty Hawk Surf Company]({filename}/images/posts/hats-sports-kitty_hawk_surf.jpg)

And while I was there, I felt compelled to get an actuall hat from one of the big surf companies.  After much searching, the one I liked best was on the sales dudes head, so I had to settle for second best, this one from [Hurley](http://www.hurley.com), another snapback.

![Hurley Surf]({filename}/images/posts/hats-sports-hurley.jpg)

