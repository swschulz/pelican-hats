Title: 1791 Arrowhead
Date: 2014-06-13 10:06
Category: Hats
Tags: 2A, Life
Slug: 1791-arrowhead
Author: Grumpy

I've been looking at the hats available from the 1791 Supply & Co. site, and while I like both of them, I thought I would start with the 1791 Arrowhead design.  It is neither over the top in terms of its statement, nor in terms of its advertising.

![1791 Arrowhead Hat]({filename}/images/posts/hats-life-1791arrowhead_600.jpg)

