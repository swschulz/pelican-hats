Title: US Soccer
Date: 2014-06-04 19:06
Category: Hats
Tags: sports, soccer
Slug: hats-us-soccer
Author: Grumpy

In support of the forthcoming [FIFA World Cup](http://www.fifa.com/worldcup/) 2014 in Brazil I stopped by [Dicks Sporting Goods](http://www.dicks.com/) and picked up one of the US Soccer hats for the collection.

![US Soccer Hat]({filename}/images/posts/hats-sports-ussoccer-1024.jpg)


