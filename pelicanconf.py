#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Grumpy Bear'
SITENAME = u'Grumpy Bear & co.'
#  Blank in here for dev/test, listed in publishconf.py for publication
SITEURL = ''

TIMEZONE = 'America/New_York'

DEFAULT_LANG = u'en'

THEMES='/Users/swschulz/Devel/pelican/pelican-themes/'
THEME= THEMES + 'pelican-bootstrap3'
BOOTSTRAP_THEME = 'superhero'

PATH = 'content'

PLUGIN_PATHS = ['/Users/swschulz/Devel/pelican/pelican-plugins']
PLUGINS = ['related_posts']

##  The following will store each article as an index.html
##  file inside a directory with the slug name, faking 
##  the clean URL look, e.g. http://site/posts/an-article-here/
##
#  Article (posts) layout
ARTICLE_URL = "posts/{date:%Y}/{date:%m}/{slug}/"
ARTICLE_SAVE_AS = ARTICLE_URL + 'index.html'
ARTICLE_PATHS = ['posts']

#  Pages layout 
PAGE_URL = '{slug}/'
PAGE_SAVE_AS = PAGE_URL + 'index.html'
PAGE_PATHS = ['pages']

CATEGORY_URL = 'category/{slug}/'
CATEGORY_SAVE_AS = CATEGORY_URL + 'index.html'
#  Theme: pelican-bootstrap3
##DISPLAY_CATEGORIES_ON_SIDEBAR = True 

TAG_URL = 'tag/{slug}/'
TAG_SAVE_AS = TAG_URL + 'index.html'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None

# Blogroll
LINKS =  (
    ('Grumpy Bear', 'http://grumpybear.co/'),
)
#    ('Python.org', 'http://python.org/'),
#    ('Jinja2', 'http://jinja.pocoo.org/'),
#    ('You can modify those links in your config file', '#'),


# Social widget
SOCIAL = (
    ('Twitter', 'http://twitter.com/ScottSchulz'),
)

DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
